Class Assignment 5 - Part 1
===================

The purpose of this assignment was to use Jenkins to create a very simple pipeline.


Setting up Jenkins
----
Jenkins was executed by running directly the WAR file with the following command: `java -jar jenkins.war`

After running this command it was possible to access Jenkins in `http://localhost:8080`.

The final step of this initial setup was to log in on Jenkins and create the credentials to access Bitbucket.

On Jenkins main menu, click on "Credentials" option and choose "Global Credentials".



Creating the pipeline
---
From the main page of Jenkins, select "New Item" and select "Pipeline".

On the configuration page of the job, in the pipeline section, select "Pipeline script from SCM" in the "Definition" dropdown.

Then, in the "Repositories" section, add the URL to the Bitbucket repository and the credentials added previously to let Jenkins access it.

The "Script Path" indicates the relative location of the Pipeline script `CA2/CA2Part1/luisnogueira-gradle_basic_demo-fef780d15427/Jenkinsfile`

This concluded the pipeline creation. The next step was to create the Jenkinsfile with the pipeline script.


Setting up the JenkinsFile
---

A Jenkinsfile with the different stages of the pipeline asked in the class assignment was created in the root folder of the project (CA2/CA2Part1/luisnogueira-gradle_basic_demo-fef780d15427/).

````
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId:'bitbucket', url:' https://jmrferreira@bitbucket.org/jmrferreira/devops-18-19-lmn-1181713/CA2/CA2Part1/'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                bat 'cd CA2/CA2Part1/luisnogueira-gradle_basic_demo-fef780d15427/; gradlew assemble'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                bat 'cd CA2/CA2Part1/luisnogueira-gradle_basic_demo-fef780d15427/; gradlew test'
            }
        }

        stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'CA2/CA2Part1/luisnogueira-gradle_basic_demo-fef780d15427/build/distributions/*'
            }
        }
    }
}
````
As required in the class assignment the Jenkinsfile included the following stages:
 1. Checkout - meant to checkout the code from Bitbucket repository. The ID of the credentials that were previously created on Jenkins for Bitbucket was used to grant access to the repository through its URL.
 2. Assemble - to compile and produce the archive files, it was used the `gradlew assemble` command in the "Assemble" stage.
 3. Test - to execute the unit tests and publish them in Jenkins, it was used the `gradlew test` command and the Junit plugin that creates a file with those results.
 4. Archive - which saves the files generated during the Assemble stage in the distributions directory.













