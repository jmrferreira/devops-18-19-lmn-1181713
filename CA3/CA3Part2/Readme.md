Class Assignment 3 - Part 2 Virtualization with Vagrant
===================

The purpose of this readme file is to detail all the features used in Class Assignment 3 - Part 2 Virtualization with Vagrant.
The main goal of this assignment was to use Vagrant to setup a virtual environment to execute the Errai Demonstration Web application.


1 Use Vagrant-Multi-Demo as Initial Solution
-------------
First of all, Vagrant was installed as recommended. With the command `Vagrant -v` we could check the installed version of Vagrant which is 2.2.4.
As required in the lecture, the project https://github.com/atb/vagrant-multi-demo was downloaded and used as base for accomplishing this assignment.

2 Study the Vagrantfile and see how it is used to create and provision 2 VMs
------------
The steps described in the readme file of vagrant multi demo were duly followed, starting by creating a folder and copying the VagrantFile of the vagrant-multi-demo project into that folder and the war file of the errai demonstration gradle project as well.
Afterwards, the `vagrant up` command was executed to create and configure virtual machines for h2 database and the Wildfly webserver with ssh access.

Then it was possible to open the errai application using the URL: http://192.168.33.10:8080/errai-demonstration-gradle/ and the H2 database using the URL: http://192.168.33.11:8082.

Even though we can access both applications there is no connection between them.

3 Update the configuration so that the Errai application uses the db VM
-------------
To work on this issue we could stop vagrant virtual machines execution with the command `vagrant halt`.

Then it was needed to update the vagrantfile in order to include the correct configuration.
The Vagrantfile was edited so that it would configure the database H2 before executing Wilfly and the following code was also added so that the Errai application uses the database:
```

        sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection -url>jdbc:h2:tcp://192.168.33.11:9092/~/test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
        sed -i 's#<password>sa</password>#<password></password>#g' /home/vagrant/wildfly-15.0.1. Final/standalone/configuration/standalone.xml 
```


After saving the Vagrantfile the Vagrant application was restarted with `vagrant reload` 
    
Now both applications are connected and we can see the contacts being persisted in H2 while both applications are still running in the following links:

    http://192.168.33.10:8080/errai-demonstration-gradle/ContactListPage
     
    http://192.168.33.11:8082/login.do?jsessionid=c3c123d24a6e3174a35903d27860ef84
        
4 Update the Vagrantfile so that the H2 database is not lost even when we destroy the VMs
--------
        
To solve this issue the Vagrantfile was updated with the following lines to persist the data in a test.mv file:
```
    db.trigger.before :halt do |trigger|
    trigger.warn = "copy to /vagrant/test.mv.db"
    trigger.run_remote = {inline: "sudo cp /root/test.mv.db /vagrant/test.mv.db"}
    end
```        

5 Final steps
-------

As required, the Vagrantfile was included in CA3 part2 repository where along with this readme.md file .
To finish this assignment, the repository was marked with the respective tag: `ca3-part2`.
