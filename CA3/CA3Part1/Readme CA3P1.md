Class Assignment 3 - Part 1 Virtualization with Vagrant
===================

The purpose of this readme file is to detail all the features used in Class Assignment 3 - Part 1 Virtualization with Vagrant.
The main goal of this assignment was to practice with Virtual Box using the same projects from the previous assignments but now inside a Virtual Box VM with Ubuntu.



1 Creating the Virtual Machine as described in the lecture
-------------

In order to create a Virtual Machine was used the virtualization platform Oracle VM Virtual Box by doing the following steps:
    
    1. Selected option "New" in Oracle VM Virtual Box Manager
    2. Afterwards, just by entering a reasonable name to the virtual machine like ‘Ubuntu’ it automatically sets the type to Linux and Version to Ubuntu 64-bit.
    3. Next was allocated 2048MB of RAM to the virtual machine and
    4. Finally, it was created a virtual disk of 10 GB with storage on physical hard disk as dynamically allocated.
    
  ##### Installing Ubuntu 18.04
  After creating the virtual machine was installed Ubuntu 18.04 by doing the following steps:
    1. Downloaded ISO image of Ubuntu 18.04 from https://help.ubuntu.com/community/Installation/MinimalCD
    2. Selected Setting option (Ctrl+S) on the Virtual Machine. 
    3. On the Storage settings menu was selected the empty slot under the IDE controller to load the ISO file with Ubuntu 18.04
    4. After that the VM was started to begin Ubuntu 18.04 installation.
    5. All the required settings during Ubuntu installation were successfully made such as choosing language, keyboard, hostname, mirror, user credentials and GRUB (bootloader).


  ##### Setup Network
  Twi network adapters were set up in the Ubuntu VM.  One as Nat and a second adapter as Host-only Adapter to grant SSH access. 
  After setting up the host-only adapter we could log on into the VM and continued the setup by doing the following steps:

  1. Installed net-tools for ifconfing: `sudo apt install net-tools`
  2. Edited network configuration to include enp0s8 network `sudo nano /etc/netplan/01-netcfg.yaml` by adding the following lines to the file and save it:
    ```
       enp0s8:
	    addresses:
	    - 192.168.56.100/24
    ```  
  3. Updated and applied changes made with `sudo netplan apply`
  4. Installed opensshh-server so that we can use ssh connections to the VM from other hosts:
  `sudo apt install openssh-server`
  5. Installed ftp server to use FTP protocol for file transfers to and from the VM with:
  `sudo apt install vsftpd` 
  6. Enabled write access for vsftpd.conf by uncommentting the line `write_enable=YES` on the file at /etc/vsftpd.conf. 
  7. Then, the service vsftpd service was restarted with `sudo service vsftpd restart`


##### Connecting to the VM from the Physical Machine with SSH connection

Now that the SSH server is configured we can now connect to the VM from our physical machine by logging in with the following login structure (username@hostOnlyIpAddress): `jferreira@192.168.56.100`
 

##### Installing Git and Java
  Still using SSH to execute the next steps, git and java were installed on the VM in order to enable repository projects execution with the following commands: 
  
  ```
  sudo apt install git
  sudo apt install openjdk-8-jdk-headless
  ```
  

#####Cloning the errai demonstration gradle
The errai-demonstration-gradle project was installed as described in the lecture by doing the following steps:
1. Cloned the project:  `git clone https://github.com/atb/errai-demonstration-gradle`
2. Build and execute the application:
    - `./gradlew provision`
    - `./gradlew build`
3. Before opening the application it was needed to edit the Wildfly standalone.xml file. 
We do this by replacing the line `<inet-address value="${jboss.bind.address:127.0.0.1}"/>` with `<any-address/>`.
We do this by replacing the line '<inet-address value="${jboss.bind.address:127.0.0.1}"/>' with '<any-address/>'.
4. Then to start the Wildfly we needed to execute `./gradlew startWildfly` and then we could successfully open the application: [Errai Contact List](http://192.168.56.100:8080/errai-demonstration-gradle).

2 Cloning the individual repository inside the VM
-------------
To clone the repository inside the VM the following command was executed: 

```
git clone https://jmrferreira@bitbucket.org/jmrferreira/devops-18-19-lmn-1181713.git
```

3 Build and execute all the projects from the previous assignments 
-----------

Since Ubuntu version installed doesn't have a graphic interface it was not possible to run some of the maven commands it wasn't possible to run CA1 inside the the VM.
In the cases of CA2 part 1 and part 2 we have to download the gradle according to the version delimited in our 'gradle/wrapper'.
With this done we were able to propperly build the application and run it.
In our case we find our VM running the client side while our browser runs the server side of the app.

4 For web projects you should access the web applications from the browser in your host machine (i.e., the "real" machine).
--------

Like the previous exercise of errai. In this case our VM runs the client side application while our browser access the server side with the correct port and url.

5 For projects such as as the simple chat application you should execute the server inside the VM and the clients in your host machine. Why is this required?
-----
This is required since the terminal we use is non-graphic.
This can be seen along the error message:

    Exception in thread "main" java.awt.HeadlessException: No X11 DISPLAY variable was set, but this program performed an operation which requires it.

As such to run the client we have to change the `build.gradle` file. More accurate the `runClient task` so that the localhost is changed to `192.168.56.101`, the IP of our VM. Then run this task accordingly.
