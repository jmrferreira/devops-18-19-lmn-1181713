package org.atb.errai.demo.contactlist;

import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.wildfly.common.Assert.assertTrue;

public class ExampleJRETest  {

	@Test
	public void creatingContactWithInvalidNameThrowsException(){
		boolean thrown = false;
		try {
			Contact contact = new Contact("Jo9ao", "joao@mail.pt", "965");
		}catch (IllegalArgumentException e) {
			thrown = true;}
		assertTrue(thrown);
	}

	@Test
	public void creatingContactWithInvalidEmailThrowsException(){
		boolean thrown = false;
		try {
			Contact contact = new Contact("Fernando Fernandes", "joaomail.pt", "965");
		}catch (IllegalArgumentException e) {
			thrown = true;}
		assertTrue(thrown);
	}

	@Test
	public void creatingContactWithInvalidPhoneThrowsException(){
		boolean thrown = false;
		try {
			Contact contact = new Contact("Fernando Fernandes", "fernandogmail.com", "9adsdadsa65");
		}catch (IllegalArgumentException e) {
			thrown = true;}
		assertTrue(thrown);
	}

	@Test
	public void getEmail(){
		Contact contact = new Contact("Fernando Fernandes", "fernandofernandes@gmail.com", "965132683");
		String expected="fernandofernandes@gmail.com";
		String result=contact.getEmail();
		assertEquals(expected,result);
	}

	@Test
	public void getName(){
		Contact contact = new Contact("Fernando Fernandes", "fernando@gmail.com", "965132683");
		String expected="Fernando Fernandes";
		String result=contact.getName();
		assertEquals(expected,result);
	}

	@Test
	public void getPhone(){
		Contact contact = new Contact("Fernando Fernandes", "fernando@gmail.com", "965132683");
		String expected="965132683";
		String result=contact.getPhone();
		assertEquals(expected,result);
	}



}
