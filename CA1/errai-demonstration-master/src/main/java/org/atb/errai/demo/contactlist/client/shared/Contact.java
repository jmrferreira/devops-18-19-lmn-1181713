package org.atb.errai.demo.contactlist.client.shared;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.jboss.errai.common.client.api.annotations.Portable;
import org.jboss.errai.databinding.client.api.Bindable;

@Bindable
@Portable
@Entity
@NamedQueries({@NamedQuery(name = Contact.ALL_CONTACTS_QUERY, query = "SELECT c FROM Contact c ORDER BY c.id")})
public class Contact implements Serializable, Comparable<Contact> {
    private static final long serialVersionUID = 1L;

    public static final String ALL_CONTACTS_QUERY = "allContacts";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String name;
    private String email;
    private String phone;

    public Contact() {
    }

    public Contact(String name, String email, String phone) {
        setName(name);
        setEmail(email);
        setPhone(phone);
    }

    public Contact(long id, String name, String email, String phone) {
        this(name, email, phone);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public boolean nameIsValid(String inputName) {
        if (inputName.matches("[A-Z][a-zA-Z ]*")) {
            return true;
        }
        throw new IllegalArgumentException("please enter a valid name.");
    }

    private void setName(String name) {
        if (nameIsValid(name))
            this.name = name;
    }

    public String getEmail() {
        return email;
    }

    /**
     * This method checks if an email address is valid. If not throws an IllegalArgumentException
     * preventing from creating an instance of a contact, as this method is invoked in constructor through
     * setEmail method.
     * @param email String parameter inputted by the user in the email field
     * @return true if email is valid, otherwise throws an IllegalArgumentException.
     */
    public boolean emailIsValid(String email) {
        if (email.matches("^[a-z0-9](\\.?[a-z0-9_-]){0,}@[a-z0-9-]+\\.([a-z]{1,6}\\.)?[a-z]{2,6}$")) {
            return true;
        }
        throw new IllegalArgumentException("please enter a valid email.");
    }

    private void setEmail(String email) {
        if (emailIsValid(email))
            this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public boolean phoneIsValid(String phone) {
        if (phone.matches("[0-9]{3,14}")) {
            return true;
        }
        throw new IllegalArgumentException("please enter a valid phone.");
    }

    private void setPhone(String phone) {
        if (phoneIsValid(phone))
            this.phone = phone;
    }


    @Override
    public String toString() {
        return "Contact [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone + "]";
    }

    @Override
    public int compareTo(Contact contact) {
        return (int) (id - contact.id);
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof Contact) && ((Contact) obj).getId() != 0 && ((Contact) obj).getId() == getId();
    }

    @Override
    public int hashCode() {
        return (int) getId();
    }
}
