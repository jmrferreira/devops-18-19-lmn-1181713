GIT - Version Control
===================
 
 The purpose of this readme file is to detail all the features used in Git as a Version Control System (VCS).
 
 
Cloning the project and initiate GIT VCS
--
This assignment required to use the errai-demonstration project that was added to bitbucket repository by executing the following git command:
 - `git clone https://github.com/atb/errai-demonstration` to clone the application from the specified directory and creates a new subdirectory named .git 
  that contains all repository files, which makes the same as `git init` in a project without VCS.
 
 
Initial Commit
-------------
 An initial commit of the application was made by executing the following git commands:
  
  -`git tag v.1.2.0` to tag the initial version to be committed
  -`git commit -a -m 'initial errai commit` in which -a argument makes git stage all the files of the project which were not yet committed. 
  (Alternatively, we could use `git commit` command for every file of the project) 
  - `git push origin v.1.2.0` to push the project to bitbucket repository tagged as v.1.2.0
  
Branches Usage 
-------------  
It's always a good practice to create and use branches for any change we want to make on our main application whether it is a hotfix, a bugfix, a new feature or a new release.
After testing the change implemented in the created branch, then it can be merged with the master branch.
  
In this project it was needed to make two different changes in the errai project. 
  
The first one was to create a phone field in the errai demonstration. The following steps were made:

* Created a new branch by executing `git phone-field` command

- Switched to that branch to implement and test the phone field creation by executing `git checkout phone-field`
- All necessary classes were updated in Java and the implemented methods were also tested. All this work was made in the new "phone-field" branch
- Once the application was stable on this branch the changes were tagged by executing `git tag v1.3.0` and then commited with `git commit -a -m 'Closes #2 created branch phone-field` in order to close the issue on bitbucket and then `git push origin v.1.2.0` to push the changes to bitbucket repository
- To switch back to master branch was executed `git checkout master` and the `git merge phone-field` so the new feature was implemented in the master branch. 
- As the phone-field branch was no longer needed, it was deleted with `git branch -d phone-field`

The same process workflow for the feature fix-invalid-email. 

  
Alternative Technological Solution
-------------  

As an alternative technology solution to Errai/GWT it could be used OpenXava (See [Open Xava](https://www.openxava.org/).) which is also an open-source
framework for development of Java Web applications.

- The latest version of OpenXava can be found at [Open Xava Download](https://sourceforge.net/projects/openxava/files/)
- The framwork is compatible with IDEs such as Eclipse
- OpenXava has Java laguage and supports maven as it has maven dependencies
- OpenXava seems also to support debugging through Maven