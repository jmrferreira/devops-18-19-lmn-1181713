Gradle Class Assignment
===================

The purpose of this readme file is to add javadoc with UML diagrams to the gradle version of errai-demonstration.


1.Download and commit Gradle Demo Project
-----------------------------------------
This assignment required to use the gradle_basic_demo that was added to bitbucket repository by executing the following git commands:
 1) `mkdir CA2Part2` to create the CA2Part1 folder inside ~/devops-18-19-lmn-1181713/ directory.
 2) `git clone https://bitbucket.org/atb/plantuml-gradle/` to clone the application from the specified URL.
 3) `-git commit -a -m 'Initial commit of plant-uml gradle example application` to put the cloned application in the staging area
 4) `-git push` to push the project to bitbucket repository


2.Created new branch
-------------------------
A new branch has been created in the repository with:

```
 git branch gradle-plantuml
```
So, after creating the new branch, it has been made the checkout to it:

````
git checkout gradle-plantuml
Switched to branch 'gradle-plantuml'
````

3.Experiment with PlantUml Gradle Application
------
The first instruction of the `README.md` was to execute:
```
./gradlew renderPlantUml
```

This task generated a puml folder in `build/puml` with image files that correspond to the puml files on `src/main/puml`. 

Then, a javadoc task has been run:

```
./gradlew javadoc
```

As JDK 11 is being used, a condition had to be added to the javadoc task:

```
javadoc {
    source = sourceSets.main.allJava
    options.overview = "src/main/javadoc/overview.html" // relative to source root
   
    if (JavaVersion.current().isJava8Compatible()) {

        tasks.withType(Javadoc) {

        // disable the crazy super-strict doclint tool in Java 8

        //noinspection SpellCheckingInspection

            options.addStringOption('Xdoclint:none',
                    '-quiet')
        }
    }
} 
```

This condition allowed to use JDK 11 and not JDK 8, the configured version.

The javadoc task generated, in `build/doc`, all the javadoc documentation of the project.



4.Experiment with PlantUml Gradle Application
------
To automate the copy of the diagrams' image files from `build/puml` into `build/docs/javadoc` it has been created a new task:

```
task copyPumlImages (type: Copy){
    from 'build/puml/com/twu/calculator'
    into 'build/docs/javadoc/com/twu/calculator'
}
```

So, these were the first steps in using Gradle.
These change on the projected was commited to the reppository:

 ```
 git commit -A -m "Creation of copy task of puml.png images to build/docs/javadoc"
 git push
 ```

5.Replicating the solution in Errai-Demonstration Application
------
A new branch has been created in the repository with:

```
 git branch ca2-part2
```
So, after creating the new branch, it has been made the checkout to it:

````
git checkout ca2-part2
Switched to branch 'ca2-part2'
````

Afterwards, the gradle version of the errai-demonstration was downloaded and commited to the repository:

 1) `git clone https://bitbucket.org/atb/errai-demonstration-gradle/` to clone the application from the specified URL.
 2) `-git commit -a -m 'Errai demonstration gradle upload` to put the cloned application in the staging area
 3) `-git push` to push the project to bitbucket repository

hen, the README.md had several instructions that have been followed.
 
 The first instruction was to run the following code:
 
 ```
 ./gradlew clean
 ```
 
 The clean task deletes a build folder that might have been previously created.
 
 
 Next, the instruction was to download and install WildFly with:
 ```
 ./gradlew provision
 ```
 
 WildFly is an application server to create web applications and provides a server environment to run them.
 
 
 After getting WildFly, the indication was to create the build folder of the project with:
 
 ```
 ./gradlew build
 ```
 
 This command created a folder with all outputs of the project and it also includes a `.war`file in `build/libs`.
 
 To start WildFly, it has been used the following code:
 
 ```
 ./gradlew startWildFly
 ``` 
This command allowed to access the application in [http://localhost:8080/errai-demonstration-gradle/ContactListPage](http://localhost:8080/errai-demonstration-gradle/ContactListPage).


Following this command, to stop WildFly execution, it has been run:

```
./gradlew stopWildFly
```


##### 5.1. Updating Gradle


To replicate the steps in the PlantUml project, the `buildSrc` folder of that project was added to Errai.

This folder has a `.groovy` file with all the code to run the renderPlantUml task.


So then, the referred task was added to the build.gradle file:

```
task renderPlantUml(type: RenderPlantUmlTask) {
}
```
Also, it has been created a puml folder in `src/main`.


To add the image files of the diagrams to this projects' build/puml folder, it has been run the task:

```
./gradlew renderPlantUml
```

After generating the images, the project documentation (class diagram and javadoc comments) was added to the Contact class.

Then, the javadoc task was executed to create the generate the files of the project's documentation:

```
./gradlew javadoc
```

After that, the copyPumlImages task has been added to this project with updated paths:

```
task copyPumlImages (type: Copy){
    from 'build/puml/org/atb/errai/demo/contactlist/client/shared'
    into 'build/docs/javadoc'
}
```

And then the task has been run to copy the images to designated folder:

```
./gradlew copyPumlImages
```

After running these last tasks, it is possible to see in the html page of javadoc files, the generated images.



##### 5.2. Committing changes

After completing all the steps, the branch were these changes were made needed to be merged with master:

```
git checkout master
git pull
git merge ca2-part2
git push
```
Then the final commit was made including a close issue command:

`-git commit -a -m 'close #15 copy images task in errai-demonstration application`