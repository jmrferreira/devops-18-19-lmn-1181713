Gradle Class Assignment
===================

The purpose of this readme file is to detail all the features used in Gradle as a build tool.


1.Download and commit Gradle Demo Project
-----------------------------------------
This assignment required to use the gradle_basic_demo that was added to bitbucket repository by executing the following git commands:
 1) `mkdir CA2Part1` to create the CA2Part1 folder inside ~/devops-18-19-lmn-1181713/ directory.
 2) `git clone https://bitbucket.org/luisnogueira/gradle_basic_demo/` to clone the application from the specified URL.
 3) `-git commit -a -m 'Initial commit	CA2Part1` to put the cloned application in the staging area
 4) `-git push` to push the project to bitbucket repository

2.Executing the gradle Demo Application
---------------------------------------
According to the instructions provided on the project's readme file, the command `./gradlew build` was executed, so that the project source code was converted into executable code in a JAR file.  
After that, the application was tested by executing the following commands on git bash:
1) `java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001` executed in a git bash terminal to initialize the chat server
2) `./gradlew runClient` executed in another git bash terminal to initialize the chat server. It was also tested to initialize 
two different client sessions with the same username but it wasn't possible as the chat server checks for duplicate names upon registration.


3.Add unit test and update gradle script
----------------------------------------
The requested unit test was created on src/test/java/basic_demo/AppTest.java class after adding 
the dependency `testCompile 'junit:junit:4.12'` on build.gradle as shown below:
```
#!java
dependencies {
    // Use Apache Log4J for logging
    compile group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    compile group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    testCompile 'junit:junit:4.12' 
}
```
 
4.Creation of copy task for src backup
----------------------------------------
For accomplyshing a backup copy of the src folder, the following task was added to build.gradle:
```
#!java
task copyDocs(type: Copy) {
    from 'src/'
    into 'backup/'
}
```
Then the following command was executed on bash to generate a copy of src folder into backup folder`./gradlew copy`. With the execution of this command the folder backup was automatically created and the content of the src folder was copied to the backup folder.


5.Creation of Zip file task of sources folder 
--------------------------------------
The following task was created to enable the creation of a zip file to archive the src folder of the application:
```
#!java
task zipArchive(type: Zip) {
    from 'src/'
    archiveName  'ProjectArquive.zip/'
}
```

The following command was executed on bash to generate the zip file`./gradlew zip`.   

Several issues were opened for the steps above and the respective commits were made.