Class Assignment 2 - Part 2
===================

Goals/Requirements
-------------

**Main Goal:** add javadoc with UML diagrams to a project

**Requirements:**

**1.** **Download and commit the example application** to personal repository

**2.** Create a **new branch** to implement the changes 

**3.** Experiment with the application based on the instructions of the README.md file

**4.** Automate the task of copying the images from build/puml to build/docs/javadoc
   
**5.** Replicate the previous solutions to the Errai project

**6.** Merge the new branch with master

**7.** Mark the repository with a new tag


Analysis, Design and Implementation of the requirements
-------------

### Getting Started

#### PlantUml Project

The first part of this assignment was based on a Gradle plantUml project, so the first files to be uploaded to the repository were from this project.

After downloading the file, all the content has been uploaded to the repository with a commit:

```
git commit -A -m "Add all plantuml-gradle-master project files"
git push
```

Then, a new branch has been created in the repository with:

```
 git branch gradle-plantuml
```

So, after creating the new branch, it has been made the checkout to it:

````
git checkout gradle-plantuml
Switched to branch 'gradle-plantuml'
````


##### Experiment with README.md

The first instruction of the `README.md` was to execute:
```
./gradlew renderPlantUml
```

This task generated a puml folder in `build/puml` with image files that correspond to the puml files on `src/main/puml`. 

Then, a javadoc task has been run:

```
./gradlew javadoc
```

As JDK 11 is being used, a condition had to be added to the javadoc task:

```
javadoc {
    source = sourceSets.main.allJava
    options.overview = "src/main/javadoc/overview.html" // relative to source root
   
    if (JavaVersion.current().isJava8Compatible()) {

        tasks.withType(Javadoc) {

        // disable the crazy super-strict doclint tool in Java 8

        //noinspection SpellCheckingInspection

            options.addStringOption('Xdoclint:none',
                    '-quiet')
        }
    }
} 
```

This condition allowed to use JDK 11 and not JDK 8, the configured version.

The javadoc task generated, in `build/doc`, all the javadoc documentation of the project.



##### New Copy Task

To automate the copy of the diagrams' image files from `build/puml` into `build/docs/javadoc` it has been created a new task:

```
task copyPumlImages (type: Copy){
    from 'build/puml/com/twu/calculator'
    into 'build/docs/javadoc/com/twu/calculator'
}
```

So, these were the first steps in using Gradle.

Next, these steps have been similarly applied to the Errai project.



#### Errai Project

##### Experiment with README.md

The Errai project files were uploaded to the repository with:
 ```
 git commit -A -m "Added all errai-demonstration-gradle files"
 git push
 ```
 
 Then, the README.md had several instructions that have been followed.
 
 The first instruction was to run the following code:
 
 ```
 ./gradlew clean
 ```
 
 The clean task deletes a build folder that might have been previously created.
 
 
 Next, the instruction was to download and install WildFly with:
 ```
 ./gradlew provision
 ```
 
 WildFly is an application server to create web applications and provides a server environment to run them.
 
 
 After getting WildFly, the indication was to create the build folder of the project with:
 
 ```
 ./gradlew build
 ```
 
 This command created a folder with all outputs of the project and it also includes a `.war`file in `build/libs`.
 
 To start WildFly, it has been used the following code:
 
 ```
 ./gradlew startWildFly
 ``` 
This command allowed to access the application in [http://localhost:8080/errai-demonstration-gradle/ContactListPage](http://localhost:8080/errai-demonstration-gradle/ContactListPage).


Following this command, to stop WildFly execution, it has been run:

```
./gradlew stopWildFly
```

##### Update Gradle

To replicate the steps in the PlantUml project, the `buildSrc` folder of that project was added to Errai.

This folder has a `.groovy` file with all the code to run the renderPlantUml task.


So then, the referred task was added to the build.gradle file:

```
task renderPlantUml(type: RenderPlantUmlTask) {
}
```
Also, it has been created a puml folder in `src/main`.


To add the image files of the diagrams to this projects' build/puml folder, it has been run the task:

```
./gradlew renderPlantUml
```

After generating the images, the project documentation (class diagram and javadoc comments) was added to the Contact class.

Then, the javadoc task was executed to create the generate the files of the project's documentation:

```
./gradlew javadoc
```

After that, the copyPumlImages task has been added to this project with updated paths:

```
task copyPumlImages (type: Copy){
    from 'build/puml/org/atb/errai/demo/contactlist/client/shared'
    into 'build/docs/javadoc'
}
```

And then the task has been run to copy the images to designated folder:

```
./gradlew copyPumlImages
```

After running these last tasks, it is possible to see in the html page of javadoc files, the generated images.


### Finishing touches

After completing all the steps, the branch were these changes were made needed to be merged with master:

```

git checkout master
git pull
git merge gradle-plantuml
git push
```


Last but not least, a tag was given to mark the assignment: 
```
git tag ca2-part2
git push origin ca2-part2
```


###Fonts
  - Overview of Build Tools - DevOps Lecture 3a  
  - Gradle - DevOps Lecture 3b
  
  - Gradle commands - 
  https://docs.gradle.org/current/userguide/command_line_interface.html#common_tasks
