Class Assignment 4
===================

The purpose of this assignment was to use Docker to setup a containerized environment to execute the Errai Demonstration Web application


Intro to Docker and Containers
-------------
According to Docker, a container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another. 
Applications are safer if encapsulated in containers and Docker provides the strongest default isolation capabilities in the industry.

Setting up Docker
---------
In order to complete this class assignment, Docker for Windows has been downloaded from `https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe` and successfully installed.
It was necessary to create an account in `docker.com`.
During the installation process it was necessary to authorize Docker.app with system account credentials, as privileged access is needed to install networking components and links to the Docker apps.

To start using Docker it was necessary to execute the `Docker.app` and then log into docker.com with previously registered account credentials.

Afterwards,  by executing the command  `docker info` on bash it was possible to view Docker system-wide information.

![Image](img/dockerInfo.png)

Docker-Compose to produce Web and Database Containers
-------
For creating a containerized environment to execute the Errai application it was necessary to setup 2 containers to create isolated environments for the Wildfly application as well as for the H2 database.

To make this happen the docker-compose-demo file structure has been used and the following changes have been made:

 - The errai-demonstration-gradle.war file was added to docker-the web container 

 - The following code has been added to the DockerFile of the WilfFly container, which provides the connection url to the h2 database as well as the user credentials to access it:

`sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \`
`sed -i 's#<password>sa</password>#<password></password>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml`

 - Also, the following command was added in order to copy the errai-demonstration-gradle.war into the container:`COPY /errai-demonstration-gradle.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war`


Then was executed the command `docker-compose build` which returned in the end the message `Successfully tagged docker_db:latest`

![](img/dockerbuild.gif)

Finally, the command `docker-compose up` was executed in order to build images from Dockerfiles as well as creating and starting the containers.

![](img/composeUp1.png)
![](img/composeUp2.png)


After accessing the web app in `http://localhost:8080/errai-demonstration-gradle/ContactListPage` and, as contacts were being created, we could see the contacts being persisted in the H2 database `http://localhost:8082` (h2:tcp://db:9092//usr/src/data/data)


![](img/contactCreation.png)


Docker Volumes
-----
Docker volumes have been specified in the `docker-compose.yml` file for the db container. Volumes specify the shared folder between the host and the container. Now, in the folder `use/src/data`, there is a file called `data.mv` in which the data is saved. This folder is a volume which 
 is defined in the docker-compose.yml file and is where the data will be saved on the host, as shown bellow:

![](img/volumes.png)

![](img/volumes1.png)

    
Publishing Docker Images
---
For publishing docker images to Docker Hub the following setps were followed:

   1.First of all, was executed the `docker login` command to authenticate to Docker Hub with user credentials.
   
   2.To check the images id of the db and web folders was inputted `docker images` command
    
   3.Then, it was tagged each image with a name preceded by the user docker id as the following example for the web folder image: `docker tag bf8369c6fa8e jmrferreira\h2database` (the same was made for the web folder image)
   
   4.To publish the image on docker hub was executed the `docker push jmrferreira\h2database` which displayed the following output:
    
    `The push refers to repository [docker.io/jmrferreira/h2database]
    dec57c88bbb3: Pushed
    608e9efcaf3c: Pushed
    b3cbc9611956: Pushed
    7660ded5319c: Pushed
    94e5c4ea5da6: Pushed
    5d74a98c48bc: Pushed
    604cbde1a4c8: Pushed
    latest: digest: sha256:c0c9c6f79668a1c970e6bb94b4094030c27f89431a531da5d7b07234a2da7a81 size: 1780`
    
    ..
![](img/dockerHub.png)

Final Commit
----
Last but not least, a tag was given to mark this assignment: 
```
git tag ca4
git push origin ca4
```