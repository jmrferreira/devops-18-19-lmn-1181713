Class Assignment 6
=========================

The purpose of this assignment was to use Ansible as an automation tool for deploying three virtual machines using Vagrant.


Deploying Ansible
----
To deploy the 3 boxes mentioned in the slides, a clone of the repository https://github.com/atb/vagrant-with-ansible was made as described. 
All boxes were started with command `vagrant up`. Then, inside /vahgrant folder on ansible box the playbook was executed with the command  `ansible-playbook playbook1.yml` to configure host1 and host2.
At this stage wildfly is already installed.

Using Ansible to configure Errai Demo App
----
To deploy and configure the errai demo application into host 1 the .war file of the errai demo application was copied into the host1 folder that contains the Vagrantfile.

Then was added a Copy task into the playbook file to copy the the errai-demonstration-gradle.war from the /vagrant
    folder to/opt/wildfly/standalone/deployments. The permissions access to the file errai .war file were also changed:
     ````
     name: Copy the errai-demonstration-gradle.war
      copy: remote_src=yes src=/vagrant/errai-demonstration-gradle.war dest=/opt/wildfly/standalone/deployments mode=0755
      ````

Then the command `vagrant up` was executed to start the VMs. Inside the ansible virtual machine in the /vagrant folder, the command `ansible-playbook playbook1.yml` was executed.

After this, the application was accessible in the url: http://192.168.33.11:8080/errai-demonstration-gradle/


Using Ansible to deploy and configure H2
----
In vagrant the order of the hosts were changed to make host2 to start first
Then, in host2 the the following lines were added to access H2 console from the host using port 8082 and to connect to the H2
       server using port 9092:
    
         host2.vm.network "forwarded_port", guest: 8082, host: 8082
         host2.vm.network "forwarded_port", guest: 9092, host: 9092
     
 In host2 the h2 was configured so that the H2 server always starts:
     
         host2.vm.provision "shell", :run => 'always', inline: <<-SHELL
           # So that the H2 server always starts
           java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
         SHELL

In the ansible VM the following changes were made in the vagrant file to install jenkins:
````
       
       	  sudo apt-get install openjdk-8-jdk-headless -y
       	  sudo wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
       	  sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
       	  sudo apt-get update -y
       	  sudo apt-get install jenkins --yes
             ````

Then jenkins was available at address http://192.168.33.10:8080/. To ensure Jenkins is securely set up by the administrator, a password has been written to the log and this file on the server: /var/lib/jenkins/secrets/initialAdminPassword
         
       

In the playbook file the following changes were also made:

- Configure download h2 database .jar from: http://repo2.maven.org/maven2/com/h2database/h2/1.4.199/h2-1.4.199.jar to
      root directory:
    
        name: Download h2 database from maven.org
        get_url: url=http://repo2.maven.org/maven2/com/h2database/h2/1.4.199/h2-1.4.199.jar dest=./h2-1.4.199.jar mode=0755

- To access H2 in the browser use link: http://192.168.33.12:8082         

- Configure errai application in host1 to use the DB2 database in host2:

- Configure errai to use the DB2 database in host2:
    name: Configure errai to use the DB2 database in host2
          replace:
            path: /opt/wildfly/standalone/configuration/standalone.xml
            regexp: '<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>'
            replace: '<connection-url>jdbc:h2:tcp://192.168.33.12:9092/~/test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>'
  
    Need to remove remove password from h2 database:
  
        name: Removing password from h2 database
          replace:
            path: /opt/wildfly/standalone/configuration/standalone.xml
            regexp: '<password>sa</password>'
            replace: '<password></password>' 
  

The pipeline of the previous assignment was updated so that this Ansible playbook could be executed at
     the end of the pipeline to deploy the new build/version of the errai application**

     it was needed to install in ansible VM git, docker with the following commands:
  
  	    sudo apt install git --yes
  	    sudo apt install docker.io --yes
  
The ansible and gradle plugins were also installed Jenkins.
    

The pipeline of ca5 was recreated in the jenkins installed in the VM machine.
  
The jenkinsfile was also changed to run with linux (see ca2\ca2Part2\errai-demonstration-gradle-master)
  
Finally the following stage was created to build ansible playbook:
  
    stage('AnsiblePlaybook') {
        steps {
            ansiblePlaybook become: true,,
            disableHostKeyChecking: true,
            inventory: 'ca6/vagrant-with-ansible-master/hosts',
            playbook: 'ca6/vagrant-with-ansible-master/playbook1.yml'
            }
        }